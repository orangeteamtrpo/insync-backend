# insync-backend

## Getting started
In order to run the application locally do the following:
* Setup a PostgreSQL database
* Initialize the database with the provided sql-script
* Add an environment variable `DATABASE_URL=postgres://USER:PASSWORD@localhost:5432/DB_NAME` (don't forget to change USER, PASSWORD and DB_NAME placeholders!)

