package org.orangeteam.insyncbackend.security;

import org.orangeteam.insyncbackend.model.Account;
import org.orangeteam.insyncbackend.repository.AccountRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.web.server.WebFilterExchange;
import org.springframework.security.web.server.authentication.RedirectServerAuthenticationSuccessHandler;
import reactor.core.publisher.Mono;

import java.time.Instant;

public class CustomAuthenticationSuccessHandler extends RedirectServerAuthenticationSuccessHandler {

    private final AccountRepository accountRepository;

    public CustomAuthenticationSuccessHandler(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Mono<Void> onAuthenticationSuccess(WebFilterExchange webFilterExchange, Authentication authentication) {
        if (authentication instanceof OAuth2AuthenticationToken oauth2Token) {
            var userInfo = oauth2Token.getPrincipal();
            var userSubject = (String) userInfo.getAttribute("sub");
            var userName = (String) userInfo.getAttribute("name");
            var userEmail = (String) userInfo.getAttribute("email");
            return accountRepository.findByEmail(userEmail)
                    .switchIfEmpty(accountRepository.save(
                            new Account(
                                    null, userName, userEmail, 1, null, userSubject,  //TODO introduce ENUM or smth for the mode
                                    oauth2Token.getAuthorizedClientRegistrationId(), Instant.now()
                            ))
                    ).then(super.onAuthenticationSuccess(webFilterExchange, authentication));
        } else {
            return super.onAuthenticationSuccess(webFilterExchange, authentication);
        }
    }
}
