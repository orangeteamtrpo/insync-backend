package org.orangeteam.insyncbackend.security.jwt;

import org.orangeteam.insyncbackend.repository.AccountRepository;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class JwtAuthenticationManager implements ReactiveAuthenticationManager {

    private final AccountRepository accountRepository;

    public JwtAuthenticationManager(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        return Mono.justOrEmpty(authentication)
                .filter(auth -> auth instanceof BearerToken)
                .cast(BearerToken.class)
                .flatMap(this::validate)
                .onErrorMap(error -> new InvalidBearerToken(error.getMessage()));
    }

    private Mono<Authentication> validate(BearerToken token) {
        var principal = token.getPrincipal();
        var email = (String) principal.getAttribute("email");
        return accountRepository.findByEmail(email)
                .switchIfEmpty(Mono.error(new IllegalArgumentException("No such user.")))
                .flatMap(user -> {
                    if (user.password() != null) {
                        return Mono.just(token);
                    } else {
                        return Mono.error(new IllegalArgumentException("Wrong auth type."));
                    }
                });
    }
}
