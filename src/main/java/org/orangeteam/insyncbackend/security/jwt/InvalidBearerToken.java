package org.orangeteam.insyncbackend.security.jwt;

import org.springframework.security.core.AuthenticationException;

public class InvalidBearerToken extends AuthenticationException {
    public InvalidBearerToken(String explanation) {
        super(explanation);
    }
}
