package org.orangeteam.insyncbackend.security.jwt;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Map;

@Component
public class JwtSupport {
    private final JwtProperties jwtProperties;
    private final SecretKey key;

    public JwtSupport(JwtProperties jwtProperties) {
        this.jwtProperties = jwtProperties;
        this.key = Keys.hmacShaKeyFor(jwtProperties.secret().getBytes());
    }

    public BearerToken generate(String username, String email) {
        var builder = Jwts.builder()
                .setClaims(Map.of("name", username, "email", email))
                .setSubject(username)
                .setIssuedAt(Date.from(Instant.now()))
                .setExpiration(Date.from(Instant.now().plus(jwtProperties.expirationInMs(), ChronoUnit.MILLIS)))
                .signWith(key);
        return new BearerToken(new JwtUser(builder.compact(), key));
    }
}
