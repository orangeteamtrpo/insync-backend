package org.orangeteam.insyncbackend.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.core.AuthenticatedPrincipal;

import javax.crypto.SecretKey;
import java.util.Map;

public class JwtUser implements AuthenticatedPrincipal {

    private final String token;
    private final Claims attributes;

    public JwtUser(String token, SecretKey key) {
        this.token = token;
        // also validates given token
        this.attributes = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody();
    }

    @Override
    public String getName() {
        return attributes.getSubject();
    }

    public Object getAttribute(String name) {
        return getAttributes().get(name);
    }

    Map<String, Object> getAttributes() {
        return attributes;
    }

    public String getToken() {
        return token;
    }
}
