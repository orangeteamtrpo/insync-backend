package org.orangeteam.insyncbackend.security.jwt;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;


public class BearerToken extends AbstractAuthenticationToken {

    private final JwtUser principal;

    public BearerToken(JwtUser principal) {
        super(AuthorityUtils.NO_AUTHORITIES);
        this.principal = principal;
        this.setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return "";
    }

    @Override
    public JwtUser getPrincipal() {
        return principal;
    }
}
