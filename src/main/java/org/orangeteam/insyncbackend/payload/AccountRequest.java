package org.orangeteam.insyncbackend.payload;

public record AccountRequest(
        String name,
        String email,
        int defaultMode
) {}
