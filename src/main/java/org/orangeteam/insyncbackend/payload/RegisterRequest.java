package org.orangeteam.insyncbackend.payload;

public record RegisterRequest(
        String username,
        String email,
        String password
) {}
