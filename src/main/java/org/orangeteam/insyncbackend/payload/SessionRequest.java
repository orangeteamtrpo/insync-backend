package org.orangeteam.insyncbackend.payload;

public record SessionRequest(
        String name,
        String[] tabs,
        int activeTab,
        int mode
) {}
