package org.orangeteam.insyncbackend.payload;

public record SessionUpdateNotification(
   long sessionId,
   String eventId
) {}
