package org.orangeteam.insyncbackend.payload;

public record AuthResponse(String token) {
}
