package org.orangeteam.insyncbackend.payload;

public record AuthRequest(
        String email,
        String password
) {}
