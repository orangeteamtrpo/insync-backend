package org.orangeteam.insyncbackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;

import java.time.Instant;

public record Session(
        @Id Long id,
        String name,
        String[] tabs,
        int activeTab,
        int mode,
        @JsonIgnore long account,
        Instant lastModified,
        Instant created
) {}
