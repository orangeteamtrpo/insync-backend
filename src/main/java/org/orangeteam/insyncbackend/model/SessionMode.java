package org.orangeteam.insyncbackend.model;

public class SessionMode {
    public static final int DETACHED_MODE = 1;
    public static final int ATTACHED_MODE = 2;
}
