package org.orangeteam.insyncbackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;

import java.time.Instant;

public record Account(
        @Id Long id,
        String name,
        String email,
        int defaultMode,
        @JsonIgnore String password,
        @JsonIgnore String oauthId,
        @JsonIgnore String authProvider,
        @JsonIgnore Instant memberSince
) {}
