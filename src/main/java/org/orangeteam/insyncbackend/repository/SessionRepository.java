package org.orangeteam.insyncbackend.repository;

import org.orangeteam.insyncbackend.model.Session;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface SessionRepository extends ReactiveCrudRepository<Session, Long> {
    Flux<Session> findAllByAccount(long accountId);

    Mono<Session> findByIdAndAccount(long id, long accountId);
}
