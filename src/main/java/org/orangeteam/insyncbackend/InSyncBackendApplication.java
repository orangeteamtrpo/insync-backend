package org.orangeteam.insyncbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan("org.orangeteam.insyncbackend")
public class InSyncBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(InSyncBackendApplication.class, args);
    }

}
