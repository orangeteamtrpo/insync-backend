package org.orangeteam.insyncbackend.handler;

import org.orangeteam.insyncbackend.model.Session;
import org.orangeteam.insyncbackend.payload.SessionUpdateNotification;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

@Component
public class EventHandler {
    private final List<Consumer<AuthenticatedEvent<SessionUpdateNotification>>> listeners = new CopyOnWriteArrayList<>();

    public void register(Consumer<AuthenticatedEvent<SessionUpdateNotification>> listener) {
        listeners.add(listener);
    }

    public void onSessionUpdate(Session updatedSession, String updateId) {
        listeners.forEach(c -> c.accept(new AuthenticatedEvent<>(
                ServerSentEvent.<SessionUpdateNotification>builder()
                        .event("session-update")
                        .data(new SessionUpdateNotification(updatedSession.id(), updateId))
                        .build(),
                updatedSession.account()
        )));
    }

    public record AuthenticatedEvent<T>(ServerSentEvent<T> event, long accountId) {}
}
