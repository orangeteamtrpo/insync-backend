package org.orangeteam.insyncbackend.handler;

import org.orangeteam.insyncbackend.model.Account;
import org.orangeteam.insyncbackend.model.SessionMode;
import org.orangeteam.insyncbackend.payload.AccountRequest;
import org.orangeteam.insyncbackend.payload.AuthRequest;
import org.orangeteam.insyncbackend.payload.AuthResponse;
import org.orangeteam.insyncbackend.payload.RegisterRequest;
import org.orangeteam.insyncbackend.repository.AccountRepository;
import org.orangeteam.insyncbackend.security.jwt.BearerToken;
import org.orangeteam.insyncbackend.security.jwt.JwtSupport;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.time.Instant;

@Component
public class AccountHandler {

    private final AccountRepository accountRepository;
    private final PasswordEncoder encoder;
    private final JwtSupport jwtSupport;

    public AccountHandler(AccountRepository accountRepository, PasswordEncoder encoder, JwtSupport jwtSupport) {
        this.accountRepository = accountRepository;
        this.encoder = encoder;
        this.jwtSupport = jwtSupport;
    }

    public Mono<AuthResponse> register(RegisterRequest request) {
        return accountRepository.findByEmail(request.email())
                .switchIfEmpty(Mono.just(new Account(
                        null,
                        request.username(),
                        request.email(),
                        SessionMode.DETACHED_MODE,
                        encoder.encode(request.password()),
                        null,
                        null,
                        Instant.now()
                )))
                .flatMap(user -> {
                    if (user.id() != null) {
                        return Mono.error(new ResponseStatusException(HttpStatus.CONFLICT));
                    } else {
                        return accountRepository.save(user).flatMap(newUser -> Mono.just(
                                new AuthResponse(
                                        jwtSupport.generate(newUser.name(), newUser.email()).getPrincipal().getToken()
                                )
                        ));
                    }
                });
    }

    public Mono<AuthResponse> login(AuthRequest request) {
        return accountRepository.findByEmail(request.email()).switchIfEmpty(
                Mono.error(new ResponseStatusException(HttpStatus.UNAUTHORIZED))
        ).flatMap(user -> {
            if (user.password() != null && encoder.matches(request.password(), user.password())) {
                return Mono.just(
                        new AuthResponse(jwtSupport.generate(user.name(), request.email()).getPrincipal().getToken())
                );
            }
            return Mono.error(new ResponseStatusException(HttpStatus.UNAUTHORIZED));
        });
    }

    public Mono<Account> getAccount(Authentication authentication) {
        String email = null;
        if (authentication instanceof OAuth2AuthenticationToken oauth2Token) {
            var userInfo = oauth2Token.getPrincipal();
            email = userInfo.getAttribute("email");
        } else if (authentication instanceof BearerToken jwtToken) {
            var userInfo = jwtToken.getPrincipal();
            email = (String) userInfo.getAttribute("email");
        }
        return accountRepository.findByEmail(email);
    }

    public Mono<Account> updateAccount(AccountRequest accountRequest, Authentication authentication) {
        return getAccount(authentication)
                .filter(account -> account.password() != null
                        || (accountRequest.name() == null || accountRequest.name().equals(account.name())
                        && account.email() == null || accountRequest.email().equals(account.email())))
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST)))
                .flatMap(account -> {
                            var name = accountRequest.name() != null ? accountRequest.name() : account.name();
                            var email = accountRequest.email() != null ? accountRequest.email() : account.email();
                            return accountRepository.save(
                                    new Account(
                                            account.id(),
                                            name,
                                            email,
                                            accountRequest.defaultMode(),
                                            account.password(),
                                            account.oauthId(),
                                            account.authProvider(),
                                            account.memberSince()
                                    )
                            );
                        }
                );
    }
}
