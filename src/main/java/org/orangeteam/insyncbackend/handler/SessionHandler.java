package org.orangeteam.insyncbackend.handler;

import org.orangeteam.insyncbackend.model.Account;
import org.orangeteam.insyncbackend.model.Session;
import org.orangeteam.insyncbackend.model.SessionMode;
import org.orangeteam.insyncbackend.payload.SessionRequest;
import org.orangeteam.insyncbackend.repository.SessionRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;

@Component
public class SessionHandler {

    private final SessionRepository sessionRepository;

    public SessionHandler(SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    public Flux<Session> getAllSessions(Account user) {
        return sessionRepository.findAllByAccount(user.id());
    }

    public Mono<Session> createSession(final SessionRequest sessionRequest, Account user) {
        if (!isValid(sessionRequest)) {
            return Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid sessionRequest object"));
        }

        Session session = new Session(
                null,
                sessionRequest.name(),
                sessionRequest.tabs(),
                sessionRequest.activeTab(),
                sessionRequest.mode(),
                user.id(),
                Instant.now(),
                Instant.now()
        );

        return sessionRepository.save(session);
    }

    public Mono<Session> getSessionById(final Long id, Account user) {
        return sessionRepository.findByIdAndAccount(id, user.id())
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "No such session")));
    }

    public Mono<Session> updateSessionById(final SessionRequest sessionRequest, final Long id, Account user) {
        if (!isValid(sessionRequest)) {
            return Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid sessionRequest object"));
        }
        return sessionRepository.findByIdAndAccount(id, user.id())
                .flatMap(session -> sessionRepository.save(
                        new Session(
                                id,
                                sessionRequest.name(),
                                sessionRequest.tabs(),
                                sessionRequest.activeTab(),
                                sessionRequest.mode(),
                                user.id(),
                                Instant.now(),
                                session.created()
                        )
                ))
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "No such session")));
    }

    public Mono<Void> deleteSessionById(final Long id, Account user) {
        return sessionRepository.findByIdAndAccount(id, user.id())
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "No such session")))
                .flatMap(session -> sessionRepository.deleteById(id));
    }

    private boolean isValid(SessionRequest sessionRequest) {
        return sessionRequest.mode() == SessionMode.DETACHED_MODE || sessionRequest.mode() == SessionMode.ATTACHED_MODE;
    }
}
