package org.orangeteam.insyncbackend.controller;

import org.orangeteam.insyncbackend.handler.AccountHandler;
import org.orangeteam.insyncbackend.model.Account;
import org.orangeteam.insyncbackend.payload.AccountRequest;
import org.orangeteam.insyncbackend.payload.AuthRequest;
import org.orangeteam.insyncbackend.payload.AuthResponse;
import org.orangeteam.insyncbackend.payload.RegisterRequest;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;


@RestController
public class AccountController {

    private final AccountHandler accountHandler;

    public AccountController(AccountHandler accountHandler) {
        this.accountHandler = accountHandler;
    }

    @PostMapping("/register")
    public Mono<AuthResponse> register(@RequestBody RegisterRequest request) {
        return accountHandler.register(request);
    }

    @PostMapping("/login")
    public Mono<AuthResponse> login(@RequestBody AuthRequest request) {
        return accountHandler.login(request);
    }

    @GetMapping("/account")
    public Mono<Account> getAccountInfo(Authentication authentication) {
        return accountHandler.getAccount(authentication);
    }

    @PutMapping("/account")
    public Mono<Account> updateAccountInfo(@RequestBody AccountRequest accountRequest, Authentication authentication) {
        return accountHandler.updateAccount(accountRequest, authentication);
    }
}
