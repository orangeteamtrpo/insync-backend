package org.orangeteam.insyncbackend.controller;

import org.orangeteam.insyncbackend.handler.AccountHandler;
import org.orangeteam.insyncbackend.handler.EventHandler;
import org.orangeteam.insyncbackend.handler.SessionHandler;
import org.orangeteam.insyncbackend.model.Session;
import org.orangeteam.insyncbackend.payload.SessionRequest;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/session")
public class SessionController {

    private final SessionHandler sessionHandler;
    private final AccountHandler accountHandler;
    private final EventHandler eventHandler;

    public SessionController(SessionHandler sessionHandler, AccountHandler accountHandler, EventHandler eventHandler) {
        this.sessionHandler = sessionHandler;
        this.accountHandler = accountHandler;
        this.eventHandler = eventHandler;
    }

    @GetMapping
    public Flux<Session> getAllSessions(Authentication auth) {
        return accountHandler.getAccount(auth).flatMapMany(sessionHandler::getAllSessions);
    }

    @PostMapping
    public Mono<Session> createSession(@RequestBody SessionRequest sessionRequest, Authentication auth) {
        return accountHandler.getAccount(auth).flatMap(user -> sessionHandler.createSession(sessionRequest, user));
    }

    @GetMapping("/{id}")
    public Mono<Session> getSessionById(@PathVariable Long id, Authentication auth) {
        return accountHandler.getAccount(auth).flatMap(user -> sessionHandler.getSessionById(id, user));
    }

    @PutMapping("/{id}")
    public Mono<Session> update(
            @RequestBody SessionRequest sessionRequest, @PathVariable Long id,
            @RequestParam String updateId, Authentication auth
    ) {
        return accountHandler.getAccount(auth)
                .flatMap(user -> sessionHandler.updateSessionById(sessionRequest, id, user))
                .map(session -> {
                    eventHandler.onSessionUpdate(session, updateId);
                    return session;
                });
    }

    @DeleteMapping("/{id}")
    public Mono<Void> deleteSessionById(@PathVariable Long id, Authentication auth) {
        return accountHandler.getAccount(auth).flatMap(user -> sessionHandler.deleteSessionById(id, user));
    }
}
