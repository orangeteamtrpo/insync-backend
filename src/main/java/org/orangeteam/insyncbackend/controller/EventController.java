package org.orangeteam.insyncbackend.controller;

import org.orangeteam.insyncbackend.handler.AccountHandler;
import org.orangeteam.insyncbackend.handler.EventHandler;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
public class EventController {

    private final EventHandler eventHandler;
    private final AccountHandler accountHandler;

    public EventController(EventHandler eventHandler, AccountHandler accountHandler) {
        this.eventHandler = eventHandler;
        this.accountHandler = accountHandler;
    }

    @SuppressWarnings("unchecked")
    @GetMapping("/events")
    public Flux<ServerSentEvent<String>> streamEvents(Authentication auth) {
        return accountHandler.getAccount(auth).flatMapMany(account ->
                Flux.create(sink -> eventHandler.register(sink::next))
                        .map(event -> (EventHandler.AuthenticatedEvent<String>) event)
                        .filter(event -> event.accountId() == account.id())
                        .map(EventHandler.AuthenticatedEvent::event));
    }
}
