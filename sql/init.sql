create table if not exists "session_mode"
(
    id smallserial
    constraint session_mode_pkey primary key,
    name varchar(32) not null
);

create table if not exists "session"
(
    id bigserial
    constraint session_pkey primary key,
    name varchar(256) not null,
    tabs text[] not null,
    active_tab integer not null,
    mode smallint not null
    constraint session_mode_fkey references "session_mode",
    account bigint not null
    constraint session_account_fkey references "account",
    last_modified timestamp with time zone not null,
    created timestamp with time zone not null
);

create table if not exists "account"
(
    id bigserial
    constraint user_pkey primary key,
    name text not null,
    email text not null,
    default_mode smallint not null
    constraint session_mode_fkey references "session_mode",
    password text,
    oauth_id text,
    auth_provider text,
    member_since timestamp with time zone not null
);

insert into session_mode (name)
values ('detached'), ('attached');